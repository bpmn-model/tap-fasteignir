#!/usr/bin/env python3
import json
import sys
import argparse
import time
import requests # pip install requests
import singer # 
import backoff
import copy

from datetime import date, datetime, timedelta

base_url = 'http://fasteignir.visir.is/api/search'

logger = singer.get_logger()
session = requests.Session()

DATE_FORMAT='%Y-%m-%d'

def validate_row(schema, row):
    # for each row we validate against the schema
    # if expected value type is not as defined in the schema we will attempt to change it
    # TODO: do this recursively if having sub properties
    # TODO: remove any attribute that should not be if schema does not allow extra properties
    maptype = {"<class 'str'>": "string", "<class 'int'>": "integer", "<class 'float'>":"number", "<class 'bool'>": "boolean", "<class 'object'>": "object", "<class 'dict'>": "object", "<class 'NoneType'>": "null"}
    # for each row validate and correct against schema
    for prop_name in schema["properties"]:
        prop_attr = schema["properties"][prop_name]
        if not prop_name in row: # missing attribute, lets set default value
            if "default" in prop_attr: row[prop_name] = prop_attr["default"]
            else: row[prop_name] = None
        # check property type (and convert if not correct)
        schema_allow_type = prop_attr["type"]
        if type (schema_allow_type) == str : schema_allow_type = [schema_allow_type]
        if not maptype[str(type(row[prop_name]))] in schema_allow_type:
            try:
                schema_allow_type_0 = schema_allow_type[0]
                if schema_allow_type_0  == "null" and len(schema_allow_type) > 1: schema_allow_type_0 = schema_allow_type[1]
                if schema_allow_type_0 == "boolean": row[prop_name] = bool(row[prop_name])
                if schema_allow_type_0 == "integer": row[prop_name] = int(row[prop_name])
                if schema_allow_type_0 == "number": row[prop_name] = float(row[prop_name])
                if schema_allow_type_0 == "string": row[prop_name] = str(row[prop_name])
            except:
                logger.info('Could not convert "' + prop_name + '" to schema type "' + schema_allow_type_0 + '" value ' + str(row[prop_name]) )
    return row



def format_response(schema, rows):
    formated_rows = [] # return rows
    for row in rows:
        # icelandic sizes uses comma
        if "size" in row: row["size"] = row["size"].replace(",", ".")
        if "openhouse" in row: del row["openhouse"]
        row = validate_row(schema, row)
        formated_rows.append(row)
    return formated_rows
    # return rows

def giveup(error):
    # logger.error(error.response.text)
    response = error.response
    return not (response.status_code == 429 or
                response.status_code >= 500)

@backoff.on_exception(backoff.constant,
                      (requests.exceptions.RequestException),
                      jitter=backoff.random_jitter,
                      max_tries=5,
                      giveup=giveup,
                      interval=30)
def request(url, params):
    response = requests.get(url=url, params=params)
    response.raise_for_status()
    return response
    
def validate_json(rows, add_date=False):
    # for each row check against schema
    for row in rows:
        print (row)

def do_sync(zip_codes, delay=4, method="list"):
    state = {'zip': zip_codes}
    schema = {}
    # load the schema file
    with open("./schema/fasteignir-schema.json") as schema_file:
        schema = json.loads(schema_file.read())

    for zip_code in zip_codes:
        try:
            full_url = base_url + "?onpage=2000&page=1&zip={zip_code}".format(zip_code=zip_code)
            logger.info('Fetching resources from URI:' + full_url)
            response = requests.get(full_url)
            payload = response.json()
            if method=="list":
                singer.write_schema('fasteignir_listing', schema, 'id')
                singer.write_records('fasteignir', format_response(schema, payload))
            else:
                schema["properties"]["updated"] = {
                    "type": "string",
                    "format": "date",
                    "default": datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ") # YYYY-MM- DDThh:mm:ssZ
                } #YYYY-MM- DDThh:mm:ssZb
                singer.write_schema('fasteignir_updates', schema, ['updated', 'id'])
                singer.write_records('fasteignir_updates', format_response(schema, payload))

            # lets wait for next request
            time.sleep(delay)

        except requests.exceptions.RequestException as e:
            logger.fatal('Error on ' + e.request.url +
                        '; received status ' + str(e.response.status_code) +
                        ': ' + e.response.text)
            singer.write_state(state)
            sys.exit(-1)
    singer.write_state(state)
    logger.info('Tap exiting normally')

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '-c', '--config', help='Config file', required=False)
    parser.add_argument(
        '-s', '--state', help='State file', required=False)

    args = parser.parse_args()

    if args.config:
        with open(args.config) as file:
            config = json.load(file)
    else:
        config = {"zip": ["101", "102"], "delay": 4, "method": "update"}

    if args.state:
        with open(args.state) as file:
            state = json.load(file)
    else:
        state = {}

    do_sync(config.get("zip"), config.get("delay"), config.get("method"))


if __name__ == '__main__':
    main()
