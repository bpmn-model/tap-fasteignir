#!/usr/bin/env python

from setuptools import setup, find_packages

setup(name='tap-fasteignir',
      version='0.1.1',
      description='Singer.io tap for extracting currency exchange rate data from the fasteignir.io API',
      author='Kjartan',
      url='http://github.com/kajjjak/tap-fasteignir',
      classifiers=['Programming Language :: Python :: 3 :: Only'],
      py_modules=['tap_fasteignir'],
      install_requires=['singer-python==2.1.4',
                        'backoff==1.3.2',
                        'requests==2.21.0'],
      extras_require={
          'dev': [
              'ipdb==0.11'
          ]
      },
      entry_points='''
          [console_scripts]
          tap-fasteignir=tap_fasteignir:main
      ''',
      packages=['tap_fasteignir'],
      include_package_data=True
)
